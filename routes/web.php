<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('materia', 'MateriaController');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Route::get('/materias', 'MateriaController@index');

Route::get('/materia/listado', function () {
    return view('materias.indexMaterias');
});

Route::get('/materia/create', function () {
    return view('materias.formMateria');
});

Route::post('/materia/store', function () {
    //validación
    //$materia = $_POST['materia'];
    //inserta a base de datos
    //redireccionar
});

Route::get('/materia/show/{id}', function ($id) {  //podemos agregar un ? después de id para indicar que es opcional
    //Buscar materia con id
    return view('materias.showMateria', compact('id'));
});

Route::get('/materia/edit/{id}', function ($id) {
  //Consultar materia con id
    return view('materias.formEditMateria', compact('id'));
});

Route::post('/materia/update/{id}', function ($id) {
    //validación
    //$materia = $_POST['materia'];
    //inserta a base de datos
    //redireccionar
});*/

//Versión mejor
/*Route::get('/materia/listado','MateriaController@index');
Route::get('/materia/create','MateriaController@create');
Route::get('/materia/store','MateriaController@store');
Route::get('/materia/edit/{materia}','MateriaController@edit');
Route::get('/materia/update/{materia}','MateriaController@update');
Route::get('/materia/show/{materia}','MateriaController@show');
Route::get('/materia/reporte-pdf','MateriaController@reportePdf');*/

//Aún mejor
Route::resource('materia', 'MateriaController');



