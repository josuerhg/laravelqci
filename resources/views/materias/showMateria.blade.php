@extends('layouts.tema')

@section('content')

<h1>
  Listado de materias
</h1>

            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Username</th>
                </tr>
              </thead>
              <tbody>
                @foreach($materias as $mat)
                <tr>
                  <td>{{ $mat->id }}</td>
                  <td>{{ $mat->nombre_materia }}</td>
                  <td>{{ $mat->crn }}</td>
                  <td>{{ $mat->salon }}</td>
                  <td>{{ $mat->horario }}</td>
                  <td>{{ $mat->calendario }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

@endsection