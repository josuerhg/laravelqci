@extends('layouts.tema')

@section('content')

<h1>
  Listado de alumnos
</h1>

            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Username</th>
                </tr>
              </thead>
              <tbody>
                @foreach($alumnos as $alumno)
                <tr>
                  <td> <a class="btn btn-sm btn-info" href="{{ route('alumno.show', $alumno->id) }}">{{ $alumno->id }}</a></td>
                  <td>{{ $alumno->nombre_materia }}</td>
                  <td>{{ $alumno->crn }}</td>
                  <td>{{ $alumno->salon }}</td>
                  <td>{{ $alumno->horario }}</td>
                  <td>{{ $alumno->calendario }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

            <!--Eliminar registro-->
            <form action="{{ route('materia.destroy', $materia->id) }}">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              <button type="submit" class="btn btn-danger">
                Eliminar
              </button>
            </form>

@endsection