<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $fillable = ['materia', 'seccion', 'crn', 'salon']; //Lo que sí puede ingresar el usuario
    //protected $guarded = ['id', 'created_at', 'updated_at'];     Lo que el usuario no puede meter (no es necesario si ya especificamos fillable)
}
